
import Foundation

/* an Indexer is the type used to actually index data, 
 * and is the type found in a linked list 
 * and maintained by leaves in the tree */

class Indexer : Keyed {
    var key: Int
    var index: Int
    var topChild: Int = 0  // never changes: never any children on a leaf 
    
    // the linked list
    var previous: Indexer?
    var next: Indexer?
    
    init(_ key: Int, _ index: Int) {
        self.key = key
        self.index = index
    }
}
