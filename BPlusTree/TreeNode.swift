//
//  ANode.swift
//  BPlusTree
//
//  Created by Ed Fallin on 10/22/19.
//  Copyright © 2019 Ed Fallin. All rights reserved.
//

import Foundation

public class TreeNode : Keyed {
    var key : Int
    
    var children : [Keyed]
    var topChild : Int
    
    init(_ key: Int) {
        self.key = key
        
        children = []
        children.reserveCapacity(GlobalConstants.fanOut)
        
        topChild = 0
    }
}
