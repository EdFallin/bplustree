
import Foundation

class BPlusTree {
    private var root: TreeNode
    
    init(_ root: TreeNode) {
        self.root = root
    }
    
    /*  ++  next up is probably what's in the tests class, followed by 
            implementing add() with overflows to pass those tests */
    
    /* each of these methods can proceed iteratively; 
     * the add() and remove() have to handle leaves differently from forks, 
     * and forks that have leaf children differently from those with fork children */
    
    func add(key: Int, index: Int) {
        /* algo: calls pathTo(), getting the path backward from indexer to root in reverse; 
         * traverses this path, inserting and indexer at the leaf, and treenodes rootward as needed, 
         * plus sloshing &| splitting nodes at overflows, including any root-add at the root */
        
        var (path, _) = pathTo(key)
        
        // first is indexer
        let newIndexer = Indexer(key, index)
        let insertAfterIndexer = path[0].node as! Indexer
        
        // splicing forward
        let nextIndexer = insertAfterIndexer.next
        newIndexer.next = nextIndexer
        
        if let next = nextIndexer {
            next.previous = newIndexer
        }
        
        // splicing backward
        insertAfterIndexer.next = newIndexer
        newIndexer.previous = insertAfterIndexer
        
        var toInsert: Keyed = newIndexer
        
        // second is leaf, then forks, then root
        for of in 1 ..< path.count {
            /* loop algorithm: 
             * the first node is the leaf before the one where the new leaf belongs, 
             * and the new leaf must be spliced into the linked list after it; 
             * at the second node (the most-leafward fork), the leaf must be added, 
             * &| if an overflow, sloshing must be done, or else splitting must start; 
             * from there rootward, anytime there is a node from the next-leafward fork, 
             * if there's an overflow, inserting &| sloshing &| splitting must be done; 
             * when there is no such node, iteration can stop; 
             * throughout, key/s and metadata about children must be updated */        
            
            let node = path[of].node as! TreeNode
            let at = path[of - 1].index  // index of ''node on parent
            
            // actually adding: inserting after the LTEBS match
            node.children.insert(toInsert, at: at + 1)
            
            // no overflow, so no more adding to do rootward
            if node.children.count <= GlobalConstants.fanOut {
                break;
            }
        }
        
        //        // leaves are handled differently and there is always one leaf, so I'm putting it here separately; 
        //        // since this is definitely a leaf, I can use the "forced-unwrapping" 'as!' instead of the iffy 'as?' 
        //        let predecessor: Indexer = path[0] as! Indexer
        //        
        //        // direct leaf handling : just splicing the leaf into the doubly-linked list: 
        //        // first, linking to any next node  
        //        newIndexer.next = predecessor.next
        //        newIndexer.next?.previous = newIndexer  // '?.' because .next may be nil 
        //        
        //        // second, linking to the previous (found) node; these are definitely not nil 
        //        predecessor.next = newIndexer
        //        newIndexer.previous = predecessor
        //        
        //        // blurring this so it can be integrated into loop and reused cleanly  
        //        var toAdd: Keyed = newIndexer
        //        
        //        for depth in 1 ..< path.count {
        //            /* loop algorithm: 
        //             * the first node is the leaf before the one where the new leaf belongs, 
        //             * and the new leaf must be spliced into the linked list after it; 
        //             * at the second node (the most-leafward fork), the leaf must be added, 
        //             * &| if an overflow, sloshing must be done, or else splitting must start; 
        //             * from there rootward, anytime there is a node from the next-leafward fork, 
        //             * if there's an overflow, inserting &| sloshing &| splitting must be done; 
        //             * when there is no such node, iteration can stop; 
        //             * throughout, key/s and metadata about children must be updated */
        //            
        //            let fork = path[depth]
        //            
        //            /* ++ the code here should be factored into several one-role methods, 
        //             *     maybe before I proceed further, maybe after */
        //            
        //            /* after I work out how I want this to work, I can possibly restore the following code as a starting point */
        //            //            // if adding would cause an overflow, first look at siblings and slosh if possible; 
        //            //            // if not possible, split the fork and distribute toAdd and siblings-to-be 
        //            //            if fork.topChild == GlobalConstants.fanOut {
        //            //                /* ++ special case must be added for root, adding a root if overflow at current root */
        //            //                
        //            //                let parent = path[depth + 1]  // siblings for sloshing are available only through parent 
        //            //            }
        //        }
    }
    
    func remove(_ key: Int) {
        /* algo: calls pathTo(), getting the path backward from leaf to root in reverse; 
         * traverses this path, deleting at the leaf and rootward as needed, 
         * plus merging forks if any underflows, including any root-drop at the root */
        
        /* loop algorithm: 
         * the first node is the leaf to be removed from the linked list, 
         * by splicing the leaves before and after it directly together; 
         * at the second node (the most-leafward fork), the leaf must be removed, 
         * and if there's an underflow, sloshing or merging with a sibling must be done; 
         * from there rootward, anytime there is an underflow,  
         * sloshing or merging with a sibling must be done; 
         * when there is no underflow, iteration can stop; 
         * throughout, key/s and metadata about children must be updated */
    }
    
    /* returns the index stored for the given key */
    func find(_ key: Int) -> Int? {
        /* algo: calls pathTo() and returns its Int value by itself, which is nil if the key was not found */
        
        let (_, index) = pathTo(key)
        return index
    }
    
    /* returns the indices stored for all keys beginning with ''start and ending with ''end */
    func find(_ start: Int, _ end: Int) -> [Int] {
        return []
    }
    
    /* algo: returns a stack of ProtoNodes and their indices from the root to the leaf for the given key, 
     * in reverse order (leaf first, root last), for tree-changing operations 
     * (even when the sought isn't found, since that is needed when changing tree), 
     * plus the sought index if an exact match was found for it */
    func pathTo(_ key: Int) -> (path: [(node: Keyed, index: Int)], index: Int?) {
        var path = [(Keyed, Int)]()
        
        path.append((root, 0))  // all paths start at the root; the 0 here is notional
        
        var node: Keyed = root
        
        var next: Int = -1
        var isExact: Bool = false
        
        // LTEBS on any children, traversing leafward, stopping 
        // when no children (at indexer), adding in reverse order
        while node is TreeNode {
            // finding next node to traverse
            let treeNode = node as! TreeNode
            (next, isExact) = BinarySearches.findInexactLess(subject: treeNode.children, sought: key)
            
            // traversing; when 'to' is an indexer, iteration ends
            node = treeNode.children[next]
            
            // retaining output, including final indexer
            path.insert((node, next), at: 0)
        }
        
        // output index is the disk location; after loop exits, definitely at an indexer
        let indexer = node as! Indexer
        let index: Int? = isExact ? indexer.index : nil
        
        return (path, index)
    }
}
