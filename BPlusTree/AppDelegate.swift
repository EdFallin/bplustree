//
//  AppDelegate.swift
//  BPlusTree
//
//  Created by Ed Fallin on 9/28/19.
//  Copyright © 2019 Ed Fallin. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

