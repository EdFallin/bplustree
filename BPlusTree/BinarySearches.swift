//
//  BinarySearches.swift
//  BPlusTree
//
//  Created by Ed Fallin on 10/13/19.
//  Copyright © 2019 Ed Fallin. All rights reserved.
//

import Foundation

class BinarySearches {
    /* a constant is coded as a 'static let' */
    static let NOT_FOUND = -1
    
    static func findInexactLess(subject: [Int], sought: Int) -> (index: Int, isExact: Bool) /* passed */ {
        /* halve and halve again, keeping the half index anytime that the at value is less than the sought */ 
        
        var low = 0
        var high = subject.count - 1
        
        var nearest = NOT_FOUND
        
        // low == half at some final search stages 
        while low <= high {
            let half = (low + high) / 2
            let at = subject[half]
            
            // exact match, search succeeded, so index returned with set flag 
            if at == sought {
                return (half, true)
            }
            
            // this try was too low, rebound next search to upward side; 
            // since also low-side inexact search, retain try-point  
            if at < sought {
                nearest = half
                low = half + 1
                continue
            }
            
            // this try was too high, rebound next search to downward side 
            if at > sought {
                high = half - 1
                continue
            }
        }
        
        // if reached, sought never found, so nearest is the result, returned with cleared flag 
        return (nearest, false)
    }
    
    static func findInexactLess(subject: [Keyed], sought: Int) -> (index: Int, isExact: Bool) /* passed */ {
        /* halve and halve again, keeping the half index anytime that the at value is less than the sought */ 
        
        var low = 0
        var high = subject.count - 1
        
        var nearest = NOT_FOUND
        
        // low == half at some final search stages 
        while low <= high {
            let half = (low + high) / 2
            
            let at = subject[half] 
            let key = at.key
            
            // exact match, search succeeded, so index returned with set flag 
            if key == sought {
                return (half, true)
            }
            
            // this try was too low, rebound next search to upward side; 
            // since also low-side inexact search, retain try-point  
            if key < sought {
                nearest = half
                low = half + 1
                continue
            }
            
            // this try was too high, rebound next search to downward side 
            if key > sought {
                high = half - 1
                continue
            }
        }
        
        // if reached, sought never found, so nearest is the result, returned with cleared flag 
        return (nearest, false)
    }
}
