//
//  Keyed.swift
//  BPlusTree
//
//  Created by Ed Fallin on 10/22/19.
//  Copyright © 2019 Ed Fallin. All rights reserved.
//

import Foundation

protocol Keyed {
    var key: Int { get set }
}
