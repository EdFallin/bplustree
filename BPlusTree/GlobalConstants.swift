//
//  GlobalConstants.swift
//  BPlusTree
//
//  Created by Ed Fallin on 9/29/19.
//  Copyright © 2019 Ed Fallin. All rights reserved.
//

import Foundation

/* this class gathers the described material in an OO-friendly way */

class GlobalConstants {
    /* this fan-out (max number of children of a fork) is very low for a real-life B+Tree */
    static let fanOut = 10
}
