//
//  BPlusTreeTests.swift
//  BPlusTreeTests
//
//  Created by Ed Fallin on 9/28/19.
//  Copyright © 2019 Ed Fallin. All rights reserved.
//

import XCTest
@testable import BPlusTree

/* ++ next up is probably to implement the add()-with-overflow test with the new wide-sample fixture, 
      inserting until a node overflows, and testing that the new key is promoted / propagated rootward */

class BPlusTreeTests: XCTestCase {
    /* fixtures */
    
    // returns a tree with indexers 0, 1, 2, 3, 4, 10, 11, 12, 13, 14, etc.,
    // for tests of searching / finding methods and simpler mutating operations
    func _buildNarrowSampleTree() -> TreeNode /* passed */ {
        // about 10 middle forks, 100 final leaves, and 500 indexers 
        
        let root = TreeNode(0)
        
        let fanOut = GlobalConstants.fanOut
        let halfFan = fanOut / 2
        
        let topFactor = fanOut * fanOut
        
        // forks: 10 of them: 0, 100, 200, ..., 900 
        for key in 0 ..< fanOut {
            let fork = TreeNode(key * topFactor)
            root.children.append(fork)
        }
        
        // leaves gathered to make it easy to add all indexers later 
        var leafwards = [TreeNode]()
        
        // retaining this for splicing linked list
        var latestIndexer : Indexer?
        
        // from forks to leaves:
        // 5 leaves for each fork, for instance 0, 10, 20, 30, 40
        for child in root.children {
            // children are defined as Keyeds but are definitely TreeNodes 
            let childFork = child as! TreeNode
            let key = childFork.key
            
            for offset in 0 ..< halfFan {
                let fork = TreeNode(key + (offset * fanOut))
                childFork.children.append(fork)
                
                // for following leaf-adding step 
                leafwards.append(fork)
            }
        }
        
        // from leaves to indexers: 
        // 5 indexers for each leaf, for instance 0, 1, 2, 3, 4
        for fork in leafwards {
            let key = fork.key
            
            for offset in 0 ..< halfFan {
                // adding indexer to leaf; 
                // same key & index is clearest 
                let indexer = Indexer(key + offset, key + offset)
                fork.children.append(indexer)
                
                // splicing together the linked list, working around 
                // optionality using Swift's syntactical "sugar"
                if let lastIndexer = latestIndexer {
                    indexer.previous = lastIndexer
                    lastIndexer.next = indexer
                }
                
                // retaining for next list-splicing
                latestIndexer = indexer
            }
        }
        
        return root
    }
    
    // returns a tree with indexers 0, 1, 2, 3, 4, 20, 21, 22, 23, 24, etc.,
    // for tests of more complicated mutating operations
    func _buildWideSampleTree() -> TreeNode {
        // about 10 middle forks, 100 final leaves, and 500 indexers 
        
        let root = TreeNode(0)
        
        let fanOut = GlobalConstants.fanOut
        let halfFan = fanOut / 2
                
        let topFactor = fanOut * fanOut
        
        // forks: 10 of them: 0, 100, 200, ..., 900 
        for key in 0 ..< fanOut {
            let fork = TreeNode(key * topFactor)
            root.children.append(fork)
        }
        
        // leaves gathered to make it easy to add all indexers later 
        var leafwards = [TreeNode]()
        
        // retaining this for splicing linked list
        var latestIndexer : Indexer?
        
        // from forks to leaves:
        // 5 leaves for each fork, for instance 0, 20, 40, 60, 80
        let forkKeyDoubling = 2

        for child in root.children {
            // children are defined as Keyeds but are definitely TreeNodes 
            let childFork = child as! TreeNode
            let key = childFork.key
            
            for offset in 0 ..< halfFan {
                let fork = TreeNode(key + (offset * fanOut * forkKeyDoubling))
                childFork.children.append(fork)
                
                // for following leaf-adding step 
                leafwards.append(fork)
            }
        }
        
        // from leaves to indexers: 
        // 5 indexers for each leaf, for instance 0, 1, 2, 3, 4
        for fork in leafwards {
            let key = fork.key
            
            for offset in 0 ..< halfFan {
                // adding indexer to leaf; 
                // same key & index is clearest 
                let indexer = Indexer(key + offset, key + offset)
                fork.children.append(indexer)
                
                // splicing together the linked list, working around 
                // optionality using Swift's syntactical "sugar"
                if let lastIndexer = latestIndexer {
                    indexer.previous = lastIndexer
                    lastIndexer.next = indexer
                }
                
                // retaining for next list-splicing
                latestIndexer = indexer
            }
        }
        
        return root
}

    /* fixture _buildNarrowSampleTree() */
    
    func test__buildNarrowSampleTree__no_conditions__assert_correct_keys() /* working */ {
        /* using a recursion-like approach for tree of known depth and shape */
        
        // preparing
        let fanOut = GlobalConstants.fanOut
        let halfFan = fanOut / 2
        
        var expected = 0
        
        
        // exercising
        let tree = _buildNarrowSampleTree()
        
        
        // gathering
        var actual = tree.key
        
        
        // comparing, with traversal
        // at root
        XCTAssertEqual(expected, actual)
        
        let root = tree
        
        // outermost loop is root, with forks by 100s, 
        // middle loop is half-full forks, with leaves by 10s,
        // innermost loop is leaves, half-full with indexers by 1s
        for forkIndex in 0 ..< fanOut {
            // expected & actual at forks
            expected = forkIndex * fanOut * fanOut  // by 100s
            actual = root.children[forkIndex].key
            
            // comparing at forks
            XCTAssertEqual(expected, actual)
            
            let fork = root.children[forkIndex] as! TreeNode  // a Keyed, but definitely a TreeNode
            
            for leafIndex in 0 ..< halfFan {
                // expected & actual at leaves
                expected = fork.key + (leafIndex * fanOut)  // by 10s
                actual = fork.children[leafIndex].key
                
                // comparing at leaves
                XCTAssertEqual(expected, actual)
                
                let leaf = fork.children[leafIndex] as! TreeNode  // known to be TreeNode
                
                for indexerIndex in 0 ..< halfFan {
                    // expected & actual at indexers
                    expected = leaf.key + indexerIndex  // by 1s
                    actual = leaf.children[indexerIndex].key  
                    
                    // comparing at indexers
                    XCTAssertEqual(expected, actual)
                    
                    // comparing at indexers: indices same as keys
                    let indexer = leaf.children[indexerIndex] as! Indexer  // known to be Indexer
                    XCTAssertEqual(expected, indexer.index)
                }
            }
        }
    }
    
    func test__buildNarrowSampleTree__no_conditions__assert_correct_forward_linked_list() /* working */ {
        /* addressing only the values at the indexers */
        
        // preparing
        let fanOut = GlobalConstants.fanOut
        let halfFan = fanOut / 2
        
        
        // exercising
        let tree = _buildNarrowSampleTree()
        
        
        // gathering 
        // path to the first indexer 
        let root = tree
        let fork = root.children[0] as! TreeNode  // known to be a non-nil TreeNode 
        let leaf = fork.children[0] as! TreeNode  // known to be a non-nil TreeNode 
        var indexer = leaf.children[0] as! Indexer  // known to be a non-nil Indexer 
        
        // loops are nested to follow underlying relationships most easily, 
        // but only the indexer keys and values are looked at 
        
        // 10 forks by 100s, each with 5 leaves by 10s, each with 5 indexers by 1s 
        for forkIndex in 0 ..< fanOut {
            let forkKey = forkIndex * fanOut * fanOut  // by 100s
            
            for leafIndex in 0 ..< halfFan {
                let leafKey = forkKey + (leafIndex * fanOut)  // by 10s
                
                for indexerIndex in 0 ..< halfFan {
                    let expected = leafKey + indexerIndex  // by 1s 
                    let actual = indexer.key
                    
                    // comparing keys 
                    XCTAssertEqual(expected, actual)
                    
                    // comparing indices, same as keys 
                    XCTAssertEqual(expected, indexer.index)
                    
                    // getting any next indexer 
                    if let next = indexer.next {
                        indexer = next
                    }
                }
            }
        }
    }
    
    func test__buildNarrowSampleTree__no_conditions__assert_correct_backward_linked_list() /* working */ {
        /* addressing only the values at the indexers */
        
        // preparing
        let fanOut = GlobalConstants.fanOut
        let halfFan = fanOut / 2
        
        // counter for comparing against expecteds later
        var at = 0
        
        var expecteds = [Int]()
        
        // gathering expecteds in forward order, reversed by inserting at 0 each time
        for forkIndex in 0 ..< fanOut {
            let forkKey = forkIndex * fanOut * fanOut  // by 100s
            
            for leafIndex in 0 ..< halfFan {
                let leafKey = forkKey + (leafIndex * fanOut)  // by 10s
                
                for indexerIndex in 0 ..< halfFan {
                    let expected = leafKey + indexerIndex  // by 1s 
                    expecteds.insert(expected, at: 0)
                }
            }
        }
        
        
        // exercising
        let tree = _buildNarrowSampleTree()
        
        
        // gathering 
        // path to the last indexer 
        let root = tree
        let fork = root.children[fanOut - 1] as! TreeNode  // known to be a non-nil TreeNode 
        let leaf = fork.children[halfFan - 1] as! TreeNode  // known to be a non-nil TreeNode 
        let last = leaf.children[halfFan - 1] as! Indexer  // known to be a non-nil Indexer 
        
        // re-aliasing as an optional for a better loop
        var indexer : Indexer? = last
        
        
        // comparing
        while indexer != nil {
            // localizing
            let expected = expecteds[at]
            let actual = indexer!.key
            
            // actual comparing
            XCTAssertEqual(expected, actual)
            
            // traversing expecteds and linked list
            at += 1
            indexer = indexer!.previous
        }
    }
    

    /* fixture _buildWideSampleTree() */
    
    func test__buildWideSampleTree__no_conditions__assert_correct_keys() /* working */ {
        /* using a recursion-like approach for tree of known depth and shape */
        
        // preparing
        let fanOut = GlobalConstants.fanOut
        let halfFan = fanOut / 2
        let forkKeyDoubling = 2
        
        var expected = 0
        
        
        // exercising
        let tree = _buildWideSampleTree()
        
        
        // gathering
        var actual = tree.key
        
        
        // comparing, with traversal
        // at root
        XCTAssertEqual(expected, actual)
        
        let root = tree
        
        // outermost loop is root, with forks by 100s, 
        // middle loop is half-full forks, with leaves by 10s,
        // innermost loop is leaves, half-full with indexers by 1s
        for forkIndex in 0 ..< fanOut {
            // expected & actual at forks
            expected = forkIndex * fanOut * fanOut  // by 100s
            actual = root.children[forkIndex].key
            
            // comparing at forks
            XCTAssertEqual(expected, actual)
            
            let fork = root.children[forkIndex] as! TreeNode  // a Keyed, but definitely a TreeNode
            
            for leafIndex in 0 ..< halfFan {
                // expected & actual at leaves
                expected = fork.key + (leafIndex * fanOut * forkKeyDoubling)  // by 20s
                actual = fork.children[leafIndex].key
                
                // comparing at leaves
                XCTAssertEqual(expected, actual)
                
                let leaf = fork.children[leafIndex] as! TreeNode  // known to be TreeNode
                
                for indexerIndex in 0 ..< halfFan {
                    // expected & actual at indexers
                    expected = leaf.key + indexerIndex  // by 1s
                    actual = leaf.children[indexerIndex].key  
                    
                    // comparing at indexers
                    XCTAssertEqual(expected, actual)
                    
                    // comparing at indexers: indices same as keys
                    let indexer = leaf.children[indexerIndex] as! Indexer  // known to be Indexer
                    XCTAssertEqual(expected, indexer.index)
                }
            }
        }
    }
    
    func test__buildWideSampleTree__no_conditions__assert_correct_forward_linked_list() /* working */ {
        /* addressing only the values at the indexers */
        
        // preparing
        let fanOut = GlobalConstants.fanOut
        let halfFan = fanOut / 2
        let forkKeyDoubling = 2
        
        
        // exercising
        let tree = _buildWideSampleTree()
        
        
        // gathering 
        // path to the first indexer 
        let root = tree
        let fork = root.children[0] as! TreeNode  // known to be a non-nil TreeNode 
        let leaf = fork.children[0] as! TreeNode  // known to be a non-nil TreeNode 
        var indexer = leaf.children[0] as! Indexer  // known to be a non-nil Indexer 
        
        // loops are nested to follow underlying relationships most easily, 
        // but only the indexer keys and values are looked at 
        
        // 10 forks by 100s, each with 5 leaves by 10s, each with 5 indexers by 1s 
        for forkIndex in 0 ..< fanOut {
            let forkKey = forkIndex * fanOut * fanOut  // by 100s
            
            for leafIndex in 0 ..< halfFan {
                let leafKey = forkKey + (leafIndex * fanOut * forkKeyDoubling)  // by 20s
                
                for indexerIndex in 0 ..< halfFan {
                    let expected = leafKey + indexerIndex  // by 1s 
                    let actual = indexer.key
                    
                    // comparing keys 
                    XCTAssertEqual(expected, actual)
                    
                    // comparing indices, same as keys 
                    XCTAssertEqual(expected, indexer.index)
                    
                    // getting any next indexer 
                    if let next = indexer.next {
                        indexer = next
                    }
                }
            }
        }
    }
    
    func test__buildWideSampleTree__no_conditions__assert_correct_backward_linked_list() /* working */ {
        /* addressing only the values at the indexers */
        
        // preparing
        let fanOut = GlobalConstants.fanOut
        let halfFan = fanOut / 2
        let forkKeyDoubling = 2

        // counter for comparing against expecteds later
        var at = 0
        
        var expecteds = [Int]()
        
        // gathering expecteds in forward order, reversed by inserting at 0 each time
        for forkIndex in 0 ..< fanOut {
            let forkKey = forkIndex * fanOut * fanOut  // by 100s
            
            for leafIndex in 0 ..< halfFan {
                let leafKey = forkKey + (leafIndex * fanOut * forkKeyDoubling)  // by 20s
                
                for indexerIndex in 0 ..< halfFan {
                    let expected = leafKey + indexerIndex  // by 1s 
                    expecteds.insert(expected, at: 0)
                }
            }
        }
        
        
        // exercising
        let tree = _buildWideSampleTree()
        
        
        // gathering 
        // path to the last indexer 
        let root = tree
        let fork = root.children[fanOut - 1] as! TreeNode  // known to be a non-nil TreeNode 
        let leaf = fork.children[halfFan - 1] as! TreeNode  // known to be a non-nil TreeNode 
        let last = leaf.children[halfFan - 1] as! Indexer  // known to be a non-nil Indexer 
        
        // re-aliasing as an optional for a better loop
        var indexer : Indexer? = last
        
        
        // comparing
        while indexer != nil {
            // localizing
            let expected = expecteds[at]
            let actual = indexer!.key
            
            // actual comparing
            XCTAssertEqual(expected, actual)
            
            // traversing expecteds and linked list
            at += 1
            indexer = indexer!.previous
        }
    }


    /* pathTo() */

    func test_pathTo__known_tree_key_present__assert_returns_correct_path() /* working */ {
        // preparing 
        let root = _buildNarrowSampleTree()
        let tree = BPlusTree(root)
        
        let key = 12
        
        let expRoot = root
        let expFork = expRoot.children[0] as! TreeNode
        let expLeaf = expFork.children[1] as! TreeNode
        let expIndexer = expLeaf.children[2] as! Indexer
        
        let expecteds: [Keyed] = [ expIndexer, expLeaf, expFork, expRoot ]
        
        
        // exercising 
        let (actuals, _) = tree.pathTo(key)
        
        
        // comparing 
        XCTAssertEqual(expecteds.count, actuals.count)
        
        for at in 0 ..< expecteds.count {
            XCTAssertEqual(expecteds[at].key, actuals[at].node.key)
        }
    }
    
    func test_pathTo__known_tree_key_present__assert_returns_correct_path_indices() /* working */ {
        // preparing
        let root = _buildNarrowSampleTree()
        let tree = BPlusTree(root)
        
        let key = 12
        
        let expRoot = 0
        let expFork = 0
        let expLeaf = 1
        let expIndexer = 2
        
        let expecteds = [ expIndexer, expLeaf, expFork, expRoot ]
        
        
        // exercising
        let (actuals, _) = tree.pathTo(key)
        
        
        // comparing
        XCTAssertEqual(expecteds.count, actuals.count)
        
        for at in 0 ..< expecteds.count {
            XCTAssertEqual(expecteds[at], actuals[at].index)
        }
    }
    
    func test_pathTo__known_tree_key_present__assert_returns_correct_index() /* working */ {
        // preparing 
        let root = _buildNarrowSampleTree()
        let tree = BPlusTree(root)
        
        let key = 12
        
        let expIndex = 12
        
        
        // exercising 
        let (_, acIndex) = tree.pathTo(key)
        
        
        // comparing 
        XCTAssertEqual(expIndex, acIndex)
    }
    
    func test_pathTo__known_tree_key_not_present__assert_returns_correct_path_and_nil_index() /* working */ {
        // preparing 
        let root = _buildNarrowSampleTree()
        let tree = BPlusTree(root)
        
        let key = 15
        
        let expRoot = root
        let expFork = expRoot.children[0] as! TreeNode
        let expLeaf = expFork.children[1] as! TreeNode
        let expIndexer = expLeaf.children[4] as! Indexer  // less-than / inexact match
        
        let expPath: [Keyed] = [ expIndexer, expLeaf, expFork, expRoot ]
        let expIndex : Int? = nil
        
        
        // exercising 
        let (acPath, acIndex) = tree.pathTo(key)
        
        
        // comparing 
        XCTAssertEqual(expPath.count, acPath.count)
        
        for at in 0 ..< expPath.count {
            XCTAssertEqual(expPath[at].key, acPath[at].node.key)
        }
        
        XCTAssertEqual(expIndex, acIndex)
    }
    
    func test_pathTo__known_tree_key_not_present__assert_returns_correct_path_indices() /* working */ {
        // preparing 
        let root = _buildNarrowSampleTree()
        let tree = BPlusTree(root)
        
        let key = 15
        
        let expRoot = 0
        let expFork = 0
        let expLeaf = 1
        let expIndexer = 4  // less-than / inexact match
        
        let expPath = [ expIndexer, expLeaf, expFork, expRoot ]
        let expIndex : Int? = nil
        
        
        // exercising 
        let (acPath, acIndex) = tree.pathTo(key)
        
        
        // comparing 
        XCTAssertEqual(expPath.count, acPath.count)
        
        for at in 0 ..< expPath.count {
            XCTAssertEqual(expPath[at], acPath[at].index)
        }
        
        XCTAssertEqual(expIndex, acIndex)
    }
    
    
    /* add() */
    
    func test_add__known_tree_end_of_array__assert_correct_path_structure() /* working */ {
        /* when a new key is added without overflow, the indexer / leaf / fork / root path is correct */
        
        // preparing
        let root = _buildNarrowSampleTree()
        let tree = BPlusTree(root)
        
        let key = 5
        let index = 5
        
        
        // exercising
        tree.add(key: key, index: index)
        
        
        // gathering results
        let fork = root.children[0] as! TreeNode
        let leaf = fork.children[0] as! TreeNode
        let indexer = leaf.children[5] as! Indexer
        
        
        // comparing
        XCTAssertEqual(10, root.children.count)  // no change here
        XCTAssertEqual(5, fork.children.count)   // no change here
        XCTAssertEqual(6, leaf.children.count)   // added here
        
        XCTAssertEqual(key, indexer.key)
        XCTAssertEqual(index, indexer.index)
    }
    
    func test_add__known_tree_end_of_array__assert_correct_indexer_links() /* working */ {
        /* when a new key is added without overflow, its indexer is correctly spliced into the linked list */
        
        // preparing
        let root = _buildNarrowSampleTree()
        let fork = root.children[0] as! TreeNode
        let leaf = fork.children[0] as! TreeNode
        let previousIndexer = leaf.children[4] as! Indexer
        let nextIndexer = previousIndexer.next!
        
        let tree = BPlusTree(root)
        
        let key = 5
        let index = 5
        
        
        // exercising
        tree.add(key: key, index: index)
        
        
        // gathering results
        let indexer = leaf.children[5] as! Indexer
        
        
        // comparing
        XCTAssertEqual(previousIndexer.key, indexer.previous!.key)
        XCTAssertEqual(nextIndexer.key, indexer.next!.key)
        
        XCTAssertEqual(key, indexer.key)
        XCTAssertEqual(index, indexer.index)
    }
    
    func test_add_known_tree_end_of_array__assert_no_key_promotions() /* working */ {
        // preparing
        let root = _buildNarrowSampleTree()
        let fork = root.children[0] as! TreeNode
        let leaf = fork.children[0] as! TreeNode
        
        let tree = BPlusTree(root)
        
        let key = 5
        let index = 5
        
        let expRootKey = root.key
        let expForkKey = fork.key
        let expLeafKey = leaf.key
        
        
        // exercising
        tree.add(key: key, index: index)
        
        
        // gathering results
        let acRootKey = root.key
        let acForkKey = fork.key
        let acLeafKey = leaf.key
        
        
        // comparing
        XCTAssertEqual(expRootKey, acRootKey)
        XCTAssertEqual(expForkKey, acForkKey)
        XCTAssertEqual(expLeafKey, acLeafKey)
    }
    
    func test_add__known_tree_start_of_array__assert_correct_key_promotions() {
        /* when a new key is added as a first key, without overflow, its key is correctly promoted back to the root */
        
        XCTAssertTrue(false, "This test is not yet finished.")
    }
}
