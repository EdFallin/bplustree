//
//  BinarySearchesTests.swift
//  BPlusTreeTests
//
//  Created by Ed Fallin on 10/13/19.
//  Copyright © 2019 Ed Fallin. All rights reserved.
//

import XCTest

@testable import BPlusTree

class BinarySearchesTests: XCTestCase {
    
    /* fixture class implementing Keyed */
    class _Keyed : Keyed {
        var key: Int
        
        init(_ key: Int) {
            self.key = key
        }
    }
    
    /* findInexactLess() over [Int] */
    
    func test_findInexactLess_Ints__soughts_do_exist__assert_returns_correct_indices_and_trues() /* working */ {
        /* when an exact match, returned flag is set */
        
        // preparing 
        let t = true
        
        let subject =       [ 1, 3, 5, 7, 9 ]
        let soughts =       [ 1, 3, 5, 7, 9 ]
        
        let expIndices  =   [ 0, 1, 2, 3, 4 ]
        let expAreExact =   [ t, t, t, t, t ]
        
        for of in 0 ..< soughts.count {
            let sought = soughts[of]
            let expIndex = expIndices[of]
            let expIsExact = expAreExact[of]
            
            // exercising 
            let (acIndex, acIsExact) = BinarySearches.findInexactLess(subject: subject, sought: sought)
            
            // comparing 
            XCTAssertEqual(expIndex, acIndex)
            XCTAssertEqual(expIsExact, acIsExact)
        }
    }
    
    func test_findInexactLess_Ints__soughts_dont_exist__assert_returns_correct_indices_and_falses() /* working */ {
        /* when an inexact (less-than), returned flag is cleared */
        
        // preparing 
        let f = false
        
        let subject =       [   1, 3, 5, 7, 9   ]
        let soughts =       [ 0, 2, 4, 6, 8, 10 ]
        
        let expIndices  =   [ -1, 0, 1, 2, 3, 4 ]  // when inexact, index of the next lower number in the subject 
        let expAreExact =   [  f, f, f, f, f, f ]
        
        for of in 0 ..< soughts.count {
            let sought = soughts[of]
            let expIndex = expIndices[of]
            let expIsExact = expAreExact[of]
            
            // exercising 
            let (acIndex, acIsExact) = BinarySearches.findInexactLess(subject: subject, sought: sought)
            
            // comparing 
            XCTAssertEqual(expIndex, acIndex)
            XCTAssertEqual(expIsExact, acIsExact)
        }
    }
    
    func test_findInexactLess_Ints__soughts_mixed_existence__assert_returns_correct_indices_and_flags() /* working */ {
        /* when an exact match, returned flag is set; when an inexact (less-than), returned flag is cleared */
        
        // preparing 
        let t = true
        let f = false
        
        let subject =       [     1,    3,    5,    7,    9     ]
        let soughts =       [  0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ]
        
        let expIndices  =   [ -1, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4  ]  // when inexact, index of the next lower number in the subject 
        let expAreExact =   [  f, t, f, t, f, t, f, t, f, t, f  ]
        
        for of in 0 ..< soughts.count {
            let sought = soughts[of]
            let expIndex = expIndices[of]
            let expIsExact = expAreExact[of]
            
            // exercising 
            let (acIndex, acIsExact) = BinarySearches.findInexactLess(subject: subject, sought: sought)
            
            // comparing 
            XCTAssertEqual(expIndex, acIndex)
            XCTAssertEqual(expIsExact, acIsExact)
        }
    }
    
    
    /* findInexactLess() over [Keyed] */
    
    func test_findInexactLess_Keyeds__soughts_do_exist__assert_returns_correct_indices_and_trues() /* working */ {
        /* when an exact match, returned flag is set */
        
        // preparing 
        let subject =       [ _Keyed(1), _Keyed(3), _Keyed(5), _Keyed(7), _Keyed(9) ]
        let soughts =       [ 1,         3,         5,         7,         9         ]
        
        let expIndices  =   [ 0,         1,         2,         3,         4         ]
        let expAreExact =   [ true,      true,      true,      true,      true      ]
        
        for of in 0 ..< soughts.count {
            let sought = soughts[of]
            let expIndex = expIndices[of]
            let expIsExact = expAreExact[of]
            
            // exercising 
            let (acIndex, acIsExact) = BinarySearches.findInexactLess(subject: subject, sought: sought)
            
            // comparing 
            XCTAssertEqual(expIndex, acIndex)
            XCTAssertEqual(expIsExact, acIsExact)
        }
    }
    
    func test_findInexactLess_Keyeds__soughts_dont_exist__assert_returns_correct_indices_and_falses() /* working */ {
        /* when an inexact (less-than), returned flag is cleared */
        
        // preparing 
        let subject =       [    _Keyed(1), _Keyed(3), _Keyed(5), _Keyed(7), _Keyed(9)    ]
        let soughts =       [  0,          2,         4,         6,         8,         10 ]
        
        // when inexact, index of the next lower number in the subject 
        let expIndices  =   [ -1,         0,         1,         2,         3,          4  ]
        let expAreExact =   [  false,     false,     false,     false,     false,  false  ]
        
        for of in 0 ..< soughts.count {
            let sought = soughts[of]
            let expIndex = expIndices[of]
            let expIsExact = expAreExact[of]
            
            // exercising 
            let (acIndex, acIsExact) = BinarySearches.findInexactLess(subject: subject, sought: sought)
            
            // comparing 
            XCTAssertEqual(expIndex, acIndex)
            XCTAssertEqual(expIsExact, acIsExact)
        }
    }
    
    func test_findInexactLess_Keyeds__soughts_mixed_existence__assert_returns_correct_indices_and_flags() /* working */ {
        /* when an exact match, returned flag is set; when an inexact (less-than), returned flag is cleared */
        
        // preparing 
        let t = true
        let f = false
        
        let subject =       [  _Keyed(1), _Keyed(3), _Keyed(5), _Keyed(7), _Keyed(9) ]
        let soughts =       [  0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ]
        
        let expIndices  =   [ -1, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4  ]  // when inexact, index of the next lower number in the subject 
        let expAreExact =   [  f, t, f, t, f, t, f, t, f, t, f ]
        
        for of in 0 ..< soughts.count {
            let sought = soughts[of]
            let expIndex = expIndices[of]
            let expIsExact = expAreExact[of]
            
            // exercising 
            let (acIndex, acIsExact) = BinarySearches.findInexactLess(subject: subject, sought: sought)
            
            // comparing 
            XCTAssertEqual(expIndex, acIndex)
            XCTAssertEqual(expIsExact, acIsExact)
        }
    }
}
